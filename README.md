# Testing how well PySpark works

## Task to accomplish by PySpark

Calculate rough estimation of population within given radius from given place. Only 10 biggest countries are included.

Read data from file http://download.geonames.org/export/dump/cities15000.zip
and unzip it to working directory.

When service is started the API is as follow:

`GET population?name=<ascii name of the place>&radius=<kilometers from that place>`

Parameters:
* `name`: ascii name of place.
* `radius`: Radius to sum population. Maximum radius is limited to 250 kilometers.

Returns: Population as integer. Returns code 400 and error message, if radius is too
  large or if name is not found.

# Install and run

ENVIRONMENT used for development:
python3 version 3.6.5
Mac book pro, MacOs High Sierra

SETUP:
Run following command line commands
```
git clone https://gitlab.com/tech-testing/pyspark-resident-counter.git
cd pyspark-resident-counter
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
FLASK_APP=population_counter.py flask run  --host=0.0.0.0 --port=5000
```

Use web browser to go to address http://127.0.0.1:5000/population?name=Thakurgaon&radius=50

# Included countries


* China
* India
* United States
* Indonesia
* Brazil
* Pakistan
* Nigeria
* Bangladesh
* Russia
* Japan
