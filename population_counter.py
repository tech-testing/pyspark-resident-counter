from flask import Flask, request
from flask import jsonify

from pyspark import SparkConf, SparkContext
from pyspark.sql import *
from pyspark.sql.functions import udf, collect_list
import json
import time

from geopy.distance import geodesic

# CONFIGURATIONS
##########
pre_process = True
number_of_cores_spark = 2
max_distance_km = 250
file_name = "cities15000.txt"

# initialze Spark and Flask
##########
app = Flask(__name__)
conf = SparkConf().set("spark.driver.maxResultSize", "1g")

conf.setMaster("local[%d]" % (number_of_cores_spark))
conf.setAppName("SparkContext1")
conf.set("spark.executor.memory", "1g")
sc = SparkContext(conf=conf)
spark = SparkSession(sc)

# Declarations of functions that need SparkContext
##########
def pre_process_data(max_distance_km, save_as = "edges"):
    @udf
    def distance_calculator(lat1, lon1, lat2, lon2):
        return geodesic((float(lat1), float(lon1)), (float(lat2), float(lon2))).kilometers

    @udf
    def zip_lists(dists, to_populations):
        # Probably some schema should be used instead of dumps.
        return json.dumps([[a, b] for a, b in zip(dists, to_populations)])

    original_file = sc.textFile(file_name)
    # splitted into columns
    vertices = original_file.map(lambda x: x.split("\t"))
    vertices = vertices.toDF(['geonameid', 'name', 'asciiname', 'alternatenames','latitude',
        'longitude','feature_class','feature_code','country_code', 'cc2','admin1_code','admin2_code',
        'admin3_code', 'admin4_code','population', 'elevation', 'dem','timezone','modification_date'])
    # take only 10 most populated countries
    vertices = vertices.filter(
        vertices.country_code.isin(['CH', 'IN', 'US', 'ID', 'BR', 'PK', 'NG', 'BD', 'RU', 'JP']))
    vertices = vertices.selectExpr("geonameid","asciiname","cast(longitude as double) longitude",
        "cast(latitude as double) latitude", "cast(population as int) population")

    # Calculate distance

    # Build edge from every locations to every locations
    to_df = vertices.selectExpr("geonameid as to_geonameid", "asciiname as to_asciiname", "longitude as to_longitude",
        "latitude as to_latitude", "population")
    from_df = vertices.selectExpr("geonameid as from_geonameid", "asciiname as from_asciiname",
        "longitude as from_longitude", "latitude as from_latitude")

    edges = from_df.crossJoin(to_df)
    edges = edges.withColumn("dist", distance_calculator(edges.from_latitude, edges.from_longitude,
        edges.to_latitude, edges.to_longitude).cast("float"))

    # aggregate distances for each location.
    edges = edges.selectExpr('from_geonameid', 'dist', 'population as to_population', 'from_asciiname')
    edges = edges.filter(edges.dist<max_distance_km)
    edges = edges.groupBy(edges.from_geonameid, edges.from_asciiname).agg(zip_lists(collect_list("dist"),
        collect_list("to_population")).alias("dist_population"))

    edges.repartition(1).write.format("json").mode("overwrite").save("edges")

def get_population_in_radius(edges, name, radius):
    found_location = edges.filter(edges.from_asciiname == name).collect()

    if(len(found_location) == 0):
        raise InvalidParam("Name %s not found" % (name))
    # After filtering there should be only one element in list.
    population_distr_str = found_location[0]['dist_population']

    # schema could be used instead of json.loads.
    population_distr = json.loads(population_distr_str)
    distance_index = 0
    population_index = 1
    inside_radius = filter(lambda x: float(x[distance_index]) < radius, population_distr)
    return sum(int(i[population_index]) for i in inside_radius)

# Main functionality
##########

start = time.time()

if(pre_process):
    pre_process_data(max_distance_km)
    pre_process_end = time.time()
    print("pre-process time", pre_process_end - start)

edges = spark.read.format("json").option("inferSchema", "true").load("edges")

end = time.time()
print("init time", end - start)

@app.route('/population', methods=['GET'])
def getPopulation():
    start = time.time()
    name = request.args.get('name')
    radius = float(request.args.get('radius'))
    if radius > max_distance_km:
        raise InvalidParam("radius must be less than %d" % (max_distance_km))

    population_sum = get_population_in_radius(edges, name, radius)

    end = time.time()
    print("query time", end - start)

    return jsonify(population_sum)

if __name__ == '__main_':
    app.run()

# Error handler
##########
class InvalidParam(Exception):
    def __init__(self, message):
        Exception.__init__(self)
        self.message = message
    def to_dict(self):
        return {'message': self.message}

@app.errorhandler(InvalidParam)
def handle_invalid_param(error):
    response = jsonify(error.to_dict())
    response.status_code = 400
    return response
